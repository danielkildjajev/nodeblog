const http = require('http');

//loome HTTP serveri objekti muutujasse "server" 
//req objekt - päring brauseri poolt koos algandmetega
//res objekt - vastus serveri poolt koos saadetavate andmetega
const server =  http.createServer((req, res) => {
    res.statusCode = 200;
    res.setHeader = ('Content-Type','text/plain');
    res.end('Hello world!\n');
});

//käivitame serveri programmi - paneme ta kuulama päringuid kindalt aadressilt ja pordilt

server.listen(3000,'127.0.0.1',()=>{
    console.log('Server is running at localhost:3000');
});